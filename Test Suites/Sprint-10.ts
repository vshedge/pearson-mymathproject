<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint-10</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>10b30bec-f8dc-4cef-b94e-ac7075316958</testSuiteGuid>
   <testCaseLink>
      <guid>4cea164e-36ff-4b0d-b0f4-631a1c5544f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyLab-Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>96071144-7261-495b-8bc5-5796e4201d35</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f44cac0c-c7a6-48ec-9e3f-39825e4d9c09</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9608b9df-880e-46b0-aa9c-2ffbe441d01c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0c9e970a-37f4-4cf4-8301-009ffae7b569</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyLab-SearchCourse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82633d65-1bfe-4072-8c0b-584918c84cda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyLab-CourseHome</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b75a626f-9ced-4c49-9af0-a48b7b3ecdcf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyLab-LogOut</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
