import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

/**
 * This test case is used to verify the MyLabs Course Login feature.
 * MyLabURL, Username and password are passed by data-files via variables.
 *
 * 	Scenario: MyLab-Login
 * 		Given The MyLab-Login page is opening
 * 		When I enter valid username 
 * 		Then I enter valid password
 * 		Then I click on the Sign-In button
 * 		Then I am at the Course Search page
 *
 */
WebUI.openBrowser('')


//WebUI.navigateToUrl(findTestData('PearsonConfig').getValue(1, 1))
WebUI.navigateToUrl(var_URL)

try {
    WebUI.click(findTestObject('page_MyLabLanding/a_Sign in'))
}
catch (NoSuchElementException e) {
    System.out.println('An exceptional case.')
} 

//WebUI.setText(findTestObject('page_MyLabSignIn/input_Username_username'), findTestData('PearsonConfig').getValue(2, 1))
WebUI.setText(findTestObject('page_MyLabSignIn/input_Username_username'),var_Username)

//WebUI.setEncryptedText(findTestObject('page_MyLabSignIn/input_Password_password'), 'p4y+y39Ir5Oy1MY8jPt0uQ==')
//WebUI.setText(findTestObject('page_MyLabSignIn/input_Password_password'), findTestData('PearsonConfig').getValue(3, 1))
WebUI.setText(findTestObject('page_MyLabSignIn/input_Password_password'),var_Password)
	
try {
    WebUI.click(findTestObject('page_MyLabSignIn/button_Sign in'))
}
catch (NoSuchElementException e) {
    System.out.println('An exceptional case.')
}





