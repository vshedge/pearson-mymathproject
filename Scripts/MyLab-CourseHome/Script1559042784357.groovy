import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/**
 * The test case is used to verify the Left Navigation Course Menu feature.
 * List of Menu Options are passed by data-files via variables.
 *
 * 	Scenario: MyLab-CourseHome
 * 		Given The MyLab-CourseHome page is opened
 * 		And I navigae to the Left Navigation Menu
 * 		And I Verify the Title
 * 		And I Click on the Menu Title
 * 		Then I get the Current course displayed
 * 
 * 	Scenario: MyLab-CourseHome
 * 		Given The MyLab-CourseHome page is opened
 * 		And I navigae to the Left Navigation Menu
 * 		And I Click on the Menu Options provided in the DataSheet
 *
 */



WebUI.waitForPageLoad(40)

WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Course Home/a_Course Home'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Course Home/a_Homework'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Homework/a_Quizzes  Tests'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Quizzes  Tests/a_Study Plan'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Study Plan/span_Gradebook'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Gradebook/a_eText'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_eText/a_Chapter Contents'), FailureHandling.CONTINUE_ON_FAILURE)



//WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Course Home/a_Course Home'))
//
//WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Course Home/a_Homework'))
//
//WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Homework/a_Quizzes  Tests'))
//
//WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Quizzes  Tests/a_Study Plan'))
//
//WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Study Plan/span_Gradebook'))
//
//WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Gradebook/a_eText'))
//
//WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_eText/a_Chapter Contents'))

WebUI.waitForPageLoad(20)

try {
	WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Chapter Contents/h2_Chapter Contents'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

try {
	WebUI.click(findTestObject('Object Repository/page_CourseContent/Page_Chapter Contents/a_Main Menu'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

try{
	WebUI.click(findTestObject('Object Repository/Page_Course Home/a_My Courses'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

//Log Out Script
WebUI.waitForPageLoad(20)


