import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/**
 * The test case is used to verify the Course Search feature.
 * CourseName is passed by data-files via variables.
 *
 * 	Scenario: MyLab-SearchCourse
 * 		Given The MyLab-Course Search page is opened
 * 		And I Click on the Active Tab
 * 		And I Enter the Course Name in the Search Textbox
 * 		And I Click on the Search Button
 * 		Then I get the searched Course
 * 
 * Scenario: MyLab-SearchCourse
 * 		Given The MyLab-Course Search page is opened
 * 		And I get the searched Course
 * 		And I Click on the searched Course
 * 		And I get the Course Home page
 * 		Then I verify the Course Title
 *
 */

	WebUI.waitForPageLoad(20)

try {
	WebUI.click(findTestObject('Object Repository/Page_Course Home/a_Active'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}
	WebUI.waitForPageLoad(20)

	WebUI.setText(findTestObject('Object Repository/Page_Course Home/input_Inactive_searchText'), findTestData('CourseName').getValue(1, 1))
	//WebUI.setText(findTestObject('Object Repository/Page_Course Home/input_Inactive_searchText'),'goncalves70908')


try {
	WebUI.click(findTestObject('Object Repository/Page_Course Home/i_Inactive_fa fa-search'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}


	WebUI.waitForPageLoad(20)

try {
	WebUI.click(findTestObject('Object Repository/Page_Course Home/div_Algebra and Trigonometry with Integrated Review 6th Edition'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}


	WebUI.waitForPageLoad(40)

try {
	//WebUI.click(findTestObject('Object Repository/Page_Course Home/svg_MyLab Math'))
	WebUI.click(findTestObject('Object Repository/Page_Course Home/svg_MyLab Math2'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}


Thread.sleep(3000)